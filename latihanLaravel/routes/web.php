<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [DashboardController::class, 'index']);

Route::get('/register', [DashboardController::class, 'biodata']);

Route::post('/welcome', [DashboardController::class, 'welcome']);

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/dashboard', function(){
    return view('dashboard');
});

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-tables', function(){
    return view('halaman.data-table');
});

//CRUD Cast

//Create
//Form Tambah Cast
Route::get('/cast/create', [CastController::class, 'create']);

Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast', [CastController::class, 'index']);

Route::get('/cast/{cast_id}', [CastController::class, 'show']);

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

Route::put('/cast/{cast_id}', [CastController::class, 'update']);

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);