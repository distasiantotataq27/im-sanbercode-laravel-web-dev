<?php

require('animal.php');
require('ape.php');
require('frog.php');

$object = new Animal("shaun");

echo "Animal Name : $object->name <br>";
echo "Legs : $object->legs <br>";
echo "Cold Blooded : $object->cold_blooded <br>";

$obj = new Frog("buduk");

echo "<br>Animal Name : $obj->name <br>";
echo "Legs : $obj->legs <br>";
echo "Cold Blooded : $obj->cold_blooded <br>";
$obj->jump();

$object2 = new Ape("kera sakti");

echo "<br><br>Animal Name : $object2->name <br>";
echo "Legs : $object2->legs <br>";
echo "Cold Blooded : $object2->cold_blooded <br>";
$object2->yell();
?>