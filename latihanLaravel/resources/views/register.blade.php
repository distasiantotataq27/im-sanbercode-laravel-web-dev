<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality:</label><br>
        <select name="region">
            <option value="indonesian">Indonesian</option><br>
            <option value="malaysian">Malaysia</option><br>
            <option value="singapore">Singapore</option><br>
            <option value="other">Other</option><br><br>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>