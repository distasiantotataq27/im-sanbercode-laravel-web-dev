<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        // Lakukan Looping Di Sini
        echo "LOOPING PERTAMA<br>";
        $a=2;
        while ($a<=20)
        {
            echo $a . " - I Love PHP<br>";
            $a+=2;
        }
        $a-=2;
        echo "LOOPING KEDUA<br>";
        while ($a>=2)
        {
            echo $a . " - I Love PHP<br>";
            $a-=2;
        }


        echo "<h3>Soal No 2 Looping Array Modulo </h3>";

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        // Lakukan Looping di sini
        foreach($numbers as $angka)
        {
            $rest[] = $angka % 5;
        }

        echo "<br>";
        echo "Array sisa baginya adalah:  ";
        print_r($rest); 
        echo "<br>";

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        // Output:
        foreach($items as $key=>$value)
        {
            $isi = array
            (
                'id'=>$value[0],
                'name'=>$value[1],
                'price'=>$value[2],
                'description'=>$value[3],
                'source'=>$value[4]
            );
            print_r($isi);
            echo "<br>";
        }
        
        echo "<h3>Soal No 4 Asterix </h3>";
        
        echo "Asterix: ";
        echo "<br>";
        
        $jumlah = 5;
        for($a=1; $a<=$jumlah; $a++)
        {
            for($b=1; $b<=$a; $b++)
            {
                echo "*";
            }
            echo "<br>";
        }
    ?>

</body>
</html>