@extends('layout.master')

@section('judul')
    Detail Cast
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<h3>{{$cast->umur}}</h3>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-success btn-sm">Kembali</a>

@endsection