<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
        return view('home');
    }

    public function biodata(){
        return view('register');
    }

    public function welcome(Request $request){
        $awal = $request->input('fname');
        $akhir = $request->input('lname');
        
        return view('welcome', ['awal'=>$awal, 'akhir'=>$akhir]);
    }
}
